# My Artefact

https://gitlab.com/hmaerki/my-artefact/-/jobs/artifacts/master/download?job=build

https://gitlab.com/hmaerki/my-artefact/-/jobs/artifacts/master/raw/helloworld?job=build


Master

  * "Hello master"
  * https://gitlab.com/hmaerki/my-artefact/-/jobs/artifacts/master/raw/helloworld.txt?job=build
  
Branch "feature XY"

* tagXY

  * "Hello Feature XY, TagXY"
  * https://gitlab.com/hmaerki/my-artefact/-/jobs/artifacts/tagXY/raw/helloworld.txt?job=build


* latest

  * "Hello Feature XY"
  * https://gitlab.com/hmaerki/my-artefact/-/jobs/artifacts/featureXY/raw/helloworld.txt?job=build


* commit hash

  * https://gitlab.com/hmaerki/my-artefact/-/jobs/artifacts/a12ef16aaa53d7ed4bb0fa18cf19db4ff337e13d/raw/helloworld.txt?job=build